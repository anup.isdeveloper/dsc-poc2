package com.dell.dsc.eps.actiontracker.util

import org.springframework.http.ResponseEntity
import org.springframework.http.HttpEntity
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component
import org.springframework.web.client.RestOperations
import org.springframework.web.client.RestTemplate

@Component
class ConsumerClientTemplate {

     var restOperations: RestOperations= RestTemplate()
     var mailServerEndpoint: String = ""

     constructor() {
       if(System.getenv("MAIL_SERVER_ENDPOINT")!=null) {
           mailServerEndpoint = System.getenv("MAIL_SERVER_ENDPOINT")
       } else {
          mailServerEndpoint = "https://mailservice-dev.ausvdc02.pcf.dell.com/infra/mail/onlybody"
       }
     }

    fun <T> executeHttpRequest(domainServerEndPoint:String,url: String, method: HttpMethod, entity: HttpEntity<T>, parameterizedTypeReference: ParameterizedTypeReference<T>): ResponseEntity<T> {
        return restOperations.exchange(domainServerEndPoint + url, method, entity, parameterizedTypeReference)
    }

    fun <T> executeHttpRequest(domainServerEndPoint:String,url: String, method: HttpMethod, entity: HttpEntity<T>) {
        restOperations.exchange(domainServerEndPoint + url, method, entity, Void::class.java)
    }

    fun executeHttpRequest(entity: HttpEntity<*>): ResponseEntity<*> {
        return restOperations.postForEntity(mailServerEndpoint, entity, String::class.java)
    }
}