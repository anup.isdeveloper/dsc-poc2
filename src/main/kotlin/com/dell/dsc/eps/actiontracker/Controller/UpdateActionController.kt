package com.dell.dsc.eps.actiontracker.Controller

import com.dell.dsc.eps.actiontracker.JsonEntity.ResponseType
import com.dell.dsc.eps.actiontracker.Model.Action
import com.dell.dsc.eps.actiontracker.Model.ActionPatch
import com.dell.dsc.eps.actiontracker.Service.ActionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.concurrent.CompletableFuture

@RestController
@RequestMapping("/api/rca/action-tracker")
@CrossOrigin("*")
class UpdateActionController {

    @Autowired
    private lateinit var actionService: ActionService

    @PostMapping("/status")
    fun updateStatus(@RequestBody updates: Action): ResponseEntity<ResponseType> {
        val updatedActions = actionService.updateAction(updates.actionId!!, updates.status)
        if (updatedActions < 1) {
            return ResponseEntity(HttpStatus.NOT_FOUND)
        }
        return ResponseEntity(ResponseType(returnMessage = "Success"), HttpStatus.CREATED)
    }

    @PostMapping("/statuses")
    fun updateMultiStatus(@RequestBody actionsUpdates: ArrayList<ActionPatch>): CompletableFuture<Boolean> {
        return actionService.updateAll(actionsUpdates)
    }

    @PostMapping("/update/action")
    fun updateAction(@RequestBody action: Action): Action {
        return actionService.updateAction(action)
    }
}