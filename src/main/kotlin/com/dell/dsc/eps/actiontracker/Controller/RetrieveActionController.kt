package com.dell.dsc.eps.actiontracker.Controller

import com.dell.dsc.eps.actiontracker.JsonEntity.FilterDropdown
import com.dell.dsc.eps.actiontracker.JsonEntity.Issues
import com.dell.dsc.eps.actiontracker.JsonEntity.PageableEntity
import com.dell.dsc.eps.actiontracker.JsonEntity.SearchCriteria
import com.dell.dsc.eps.actiontracker.Model.Action
import com.dell.dsc.eps.actiontracker.Service.ActionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*
import kotlin.collections.HashMap

@RestController
@RequestMapping("/api/rca/action-tracker")
@CrossOrigin("*")
class RetrieveActionController {

    @Autowired
    private lateinit var actionService: ActionService

    @PostMapping("/action/actions")
    fun getActions(@RequestBody(required = false) searchCriteria: SearchCriteria): PageableEntity {
        return actionService.getAllAction(searchCriteria)
    }

    @GetMapping("/action/{actionId}")
    fun getActionById(@RequestParam actionId: String): Optional<Action> {
        return actionService.findActionById(actionId)
    }

    @PostMapping("/action/issue")
    fun getActionsForOneIssueId(@RequestBody issue: Issues): Iterable<Action> {
        return actionService.findActionByIssueId(issue.issueId)
    }


    @PostMapping("/action/issues")
    fun getActionsForMultipleIssueId(@RequestBody issueIds: List<String>): HashMap<String, Iterable<Action>> {
        return actionService.findByMultipleIssueId(issueIds)
    }

    @GetMapping("/action/assigner")
    fun getAllUniqueAssigner(): List<FilterDropdown> {
        return actionService.findUniqueAssigner()
    }
}