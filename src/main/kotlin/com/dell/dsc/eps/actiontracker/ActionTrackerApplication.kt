package com.dell.dsc.eps.actiontracker

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.core.task.TaskExecutor
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@EnableAsync
@SpringBootApplication
class ActionTrackerApplication

fun main(args: Array<String>) {
    runApplication<ActionTrackerApplication>(*args)
}

@Bean
fun threadPoolTaskExecutor(): TaskExecutor {
    val executor = ThreadPoolTaskExecutor()
    executor.corePoolSize = Runtime.getRuntime().availableProcessors() - 1
    executor.maxPoolSize = Runtime.getRuntime().availableProcessors() - 1
    executor.setThreadNamePrefix("default_task_executor_thread")
    executor.initialize()
    return executor
}

/*@Bean
fun corsConfigurer(): WebMvcConfigurer {
    return object : WebMvcConfigurer {
       override fun addCorsMapping(registry: CorsRegistry?) {
            registry!!.addMapping("/api/**").allowedOrigins("*")
        }
    }
}*/