package com.dell.dsc.eps.actiontracker.Model

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
 class PartIssueDetailsId: Serializable {

    @Column(name = "issue_id")
    lateinit var issueId: String;
    @Column(name="use_case")
    lateinit var useCase: String;

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PartIssueDetailsId

        if (issueId != other.issueId) return false
        if (useCase != other.useCase) return false

        return true
    }

    override fun hashCode(): Int {
        var result = issueId.hashCode()
        result = 31 * result + useCase.hashCode()
        return result
    }
}