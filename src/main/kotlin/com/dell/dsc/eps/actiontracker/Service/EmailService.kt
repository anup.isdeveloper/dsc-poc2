package com.dell.dsc.eps.actiontracker.Service

import com.dell.dsc.eps.actiontracker.JsonEntity.MailData
import com.dell.dsc.eps.actiontracker.Model.Action
import com.dell.dsc.eps.actiontracker.util.ConsumerClientTemplate
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.core.io.FileSystemResource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

@Service
class EmailService(var consumerClientTemplate: ConsumerClientTemplate) {

    fun triggerEmail(mailData: MailData): ResponseEntity<*> {

        val headers = HttpHeaders()
        var jsonFile: File? = null
        val objectMapper = ObjectMapper()
        headers.contentType = MediaType.MULTIPART_FORM_DATA

        try {
            jsonFile = File.createTempFile("mailData", ".json")
            jsonFile.writeText(objectMapper.writeValueAsString(mailData))

        } catch (e: IOException) {
            println("Cannot create file")
        }

        val body = LinkedMultiValueMap<String, Any>()
        body.add("maildata", FileSystemResource(jsonFile!!))
        val requestEntity = HttpEntity<MultiValueMap<String, Any>>(body, headers)
        return consumerClientTemplate.executeHttpRequest(requestEntity)
    }

    fun getActionRowTemplate(action: Action): String {
        return getRowString(action.issueId, action.actionId!!, action.actionDescription, action.dueDate, action.status)
    }

    fun getActionRowTemplate(actions: ArrayList<Action>): String {
        var allActionRows: String = ""
        actions.forEach { action ->
            allActionRows += getRowString(action.issueId, action.actionId!!, action.actionDescription,
                    action.dueDate, action.status)
        }
        return allActionRows
    }

    fun getRowString(someIssueId: String, someActionId: String, someActionDesc: String, someDueDate: Long,
                     someStatusForAction: String): String {
        val actionIdSplit: List<String> = someActionId.split("/")
        val formatter = SimpleDateFormat("MMM dd, yyyy")
        val actionIdPostSplit = actionIdSplit[actionIdSplit.size - 1]
        val dueDate = formatter.format(Date(someDueDate))
        println("Due date for email template = " + dueDate)
        println("Due date in milisecound  = " + someDueDate)
        return "<tr style='text-align: center'> <td style='font-size: 15px;padding: 10px 5px;'>$someIssueId</td>" +
                "<td style='font-size: 15px;padding: 10px 5px;'>$actionIdPostSplit</td> <td style='font-size: 15px;" +
                "padding: 10px 5px;'>$someActionDesc</td> <td style='font-size: 15px;padding: 10px 5px;'>$dueDate</td>" +
                "<td style='font-size: 15px;padding: 10px 5px;'>$someStatusForAction</td> </tr>"
    }

    fun getEmailBody(actionRows: String, actionTrackerLink: String): String {
        val emailBody: String = "<body style='text-align: -webkit-center;'>\n" +
                "\n" +
                "<div style='text-align: center; width: 1000px; border: 1px solid #8dacc5;  padding-bottom: 50px;'>\n" +
                "    <div style='background-color: #00447c; height: 100px '>\n" +
                "        <div style='padding-top: 30px'>\n" +
                "            <span style='color: #fff; font-size: 32px'>Root Cause & Action Engine</span>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div style='padding-top: 20px'>\n" +
                "        <table style='width: 100%'>\n" +
                "            <thead>\n" +
                "            <tr>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>ISSUE ID</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>ACTION ID</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>ACTION</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>DUE DATE</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>STATUS</th>\n" +
                "            </tr>\n" +
                "            </thead>\n" +
                "            <tbody>$actionRows</tbody>\n" +
                "        </table>\n" +
                "\n" +
                "    </div>\n" +
                "\n" +
                "    <div style='padding-top: 50px'>\n" +
                "        <div style='text-align: center'>\n" +
                "            <button style='padding: 10px 20px; font-size: 14px; background-color: #00447c !important; border: none; border-radius: 3px;'>\n" +
                "                <a style='color: white !important;text-decoration: none;' href='$actionTrackerLink'>Goto Action View</a>\n" +
                "            </button>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n" +
                "</body>"
        return emailBody
    }
}