package com.dell.dsc.eps.actiontracker.JsonEntity

class ActionEntity(
        val actionId: String? = "",
        val issueId: String,
        val actionDescription: String,
        val classification: String,
        val sourceApp: String? = "",
        val issueType: String? = "",
        val status: String = "OPEN",
        val assignedTo: String,
        val dueDate: Long,
        val lastUpdatedDate: Long,
        val createDate: Long,
        val comments: String? = ""
) {}