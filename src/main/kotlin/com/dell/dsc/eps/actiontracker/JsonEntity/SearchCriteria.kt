package com.dell.dsc.eps.actiontracker.JsonEntity

import java.util.*

class SearchCriteria(
        var startDate: Long? =null,
        var endDate: Long? = null,
        var actionId: String? = null,
        var issueId: String? = null,
        var emailId: String? = null,
        var sourceApp: String? = null,
        var status: String? = null,
        var pageNo: Int? =0,
        var pageSize: Int? = 50
) {
    fun isDateEmpty(): Boolean {
        return  (this.startDate == null || this.endDate == null)
    }
}