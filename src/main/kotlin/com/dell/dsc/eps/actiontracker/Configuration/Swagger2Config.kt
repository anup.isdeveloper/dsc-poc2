package com.dell.dsc.eps.actiontracker.Configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.service.AuthorizationScope
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2
import springfox.documentation.service.ApiKey
import java.util.*
import springfox.documentation.service.SecurityReference
import kotlin.collections.ArrayList


@Configuration
@EnableSwagger2
class Swagger2Config {

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**")).build().pathMapping("/")
                .securitySchemes(accessToken())
                .securityContexts(Arrays.asList(securityContext()));

    }

    private fun accessToken(): ArrayList<ApiKey> {
        val authHeader = ArrayList<ApiKey>()
        authHeader.add(ApiKey("user-email", "user-email", "header"))
        authHeader.add(ApiKey("Authorization", "Authorization", "header"))
        return authHeader
    }

    private fun securityContext(): SecurityContext {
        return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/api.*"))
                .build()
    }

    private fun defaultAuth(): List<SecurityReference> {
        val authorizationScope = AuthorizationScope("global", "accessEverything")
        val authorizationScopes = arrayOfNulls<AuthorizationScope>(1)
        authorizationScopes[0] = authorizationScope
        return Arrays.asList(
                SecurityReference("Authorization", authorizationScopes),
                SecurityReference("user-email", authorizationScopes)
        )
    }
}
