package com.dell.dsc.eps.actiontracker.Model

import java.util.*
import kotlin.collections.ArrayList

class ActionPatch(
        val issueId: String = "",
        val actionPatch: ArrayList<ActionPatchObj>
       ) {
       class ActionPatchObj(
               val actionId: String,
               val status: String = "OPEN"
       ){}
}