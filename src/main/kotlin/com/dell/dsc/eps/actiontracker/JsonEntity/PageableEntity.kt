package com.dell.dsc.eps.actiontracker.JsonEntity

class PageableEntity (var pageSize: Int, var totalElements : Long, var offset: Long, var pageNumber: Int, var content: List<Any> ) {
}