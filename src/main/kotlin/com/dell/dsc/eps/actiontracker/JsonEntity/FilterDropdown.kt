package com.dell.dsc.eps.actiontracker.JsonEntity

class FilterDropdown (
    var label: String,
    var value: String
)