package com.dell.dsc.eps.actiontracker.Controller

import com.dell.dsc.eps.actiontracker.JsonEntity.ActionEntity
import com.dell.dsc.eps.actiontracker.Model.Action
import com.dell.dsc.eps.actiontracker.Service.ActionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/rca/action-tracker")
@CrossOrigin("*")
class CreateActionController {

    @Autowired
    private lateinit var actionService: ActionService

    @PostMapping("/action")
    @ResponseStatus(HttpStatus.CREATED)
    fun createAction(@RequestBody action: ActionEntity, @RequestParam timeOffset: Int): Action {
        return actionService.saveAction(action, timeOffset)
    }

    @PostMapping("/actions")
    @ResponseStatus(HttpStatus.CREATED)
    fun createMultiAction(@RequestBody actions: List<ActionEntity>, @RequestParam timeOffset: Int): Iterable<Action> {
        return actionService.saveMultiAction(actions, timeOffset)
    }
}

