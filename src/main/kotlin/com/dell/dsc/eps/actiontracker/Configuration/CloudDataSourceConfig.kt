package com.dell.dsc.eps.actiontracker.Configuration


import org.apache.commons.dbcp.BasicDataSource
import org.springframework.cloud.config.java.AbstractCloudConfig
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

import javax.sql.DataSource

class CloudDataSourceConfig {
    @Configuration
    @Profile("cloud")
    internal class CloudConfiguration: AbstractCloudConfig() {
        @Bean("memsql-db")
        fun dataSource(): DataSource {
            return connectionFactory().dataSource()
        }
    }

    @Configuration
    @Profile("default")
    internal class LocalConfiguration {
        @Bean("local-db")
        fun dataSource(): DataSource {
            val dataSource = BasicDataSource()
            dataSource.setUrl("jdbc:mysql://ddldlmem201.us.dell.com:3306/dsc_procure" +
                    "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC")
            dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver")
            dataSource.setUsername("svc_dsc_procure_rw")
            dataSource.setPassword("svc4rwpro")
            return dataSource
        }
    }

    @Configuration
    @Profile("local")
    internal class LocalDbConfiguration {
        @Bean("local-db")
        fun dataSource(): DataSource {
            val dataSource = BasicDataSource()
            dataSource.setUrl("jdbc:mysql://localhost:3306/dsc_procure")
            dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver")
            dataSource.setUsername("partsy")
            dataSource.setPassword("oneP@rtsy!123")
            return dataSource
        }
    }
}
