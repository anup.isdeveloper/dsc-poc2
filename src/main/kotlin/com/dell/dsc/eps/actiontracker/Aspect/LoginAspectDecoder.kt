package com.dell.dsc.eps.actiontracker.Aspect

import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.springframework.stereotype.Component
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import com.auth0.jwt.interfaces.Header;
import com.auth0.jwt.interfaces.Payload;
import com.auth0.jwt.impl.JWTParser
import com.auth0.jwt.exceptions.JWTDecodeException

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import com.auth0.jwt.interfaces.Claim


@Aspect
@Component
class LoginAspectDecoder {

    private var payload: Payload? = null
    private var loggedInUserEmail: String = ""

    @Before("execution(* com.dell.dsc.eps.actiontracker.Controller.*.createMultiAction(*,*))" +
            "|| execution(* com.dell.dsc.eps.actiontracker.Controller.*.createAction(*,*))" +
            "|| execution(* com.dell.dsc.eps.actiontracker.Controller.*.updateAction(*))")
    fun tokenExtract() {
        payload = null
        loggedInUserEmail = ""
        val request = (RequestContextHolder.getRequestAttributes() as ServletRequestAttributes).request
        val accessToken = request.getHeader("Authorization")
        if (accessToken != null) {
            if (accessToken.contains("Bearer")) {
                jwtDecoder(JWTParser(), accessToken.substring("Bearer&".length))
            }
        } else {
            loggedInUserEmail = request.getHeader("user-email")
        }
    }

    @Throws(JWTDecodeException::class)
    fun jwtDecoder(converter: JWTParser, jwt: String) {
        val parts = jwt.split(".") as ArrayList<String>
        val headerJson: String
        val payloadJson: String
        try {
            headerJson = StringUtils.newStringUtf8(Base64.decodeBase64(parts.get(0)))
            payloadJson = StringUtils.newStringUtf8(Base64.decodeBase64(parts.get(1)))
        } catch (e: NullPointerException) {
            throw JWTDecodeException("The UTF-8 Charset isn't initialized.", e)
        }
        val header: Header = converter.parseHeader(headerJson)
        payload = converter.parsePayload(payloadJson)
    }

    fun getClaim(name: String): Claim {
        return payload!!.getClaim(name)
    }

    fun getLoggedInUserEmail(): String {
        if (payload != null) {
            return getClaim("email").asString().toLowerCase()
        } else {
            return loggedInUserEmail.toLowerCase()
        }
    }

    fun getClaims(): Map<String, Claim> {
        return payload!!.getClaims()
    }
}