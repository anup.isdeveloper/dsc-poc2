package com.dell.dsc.eps.actiontracker.Service

import com.dell.dsc.eps.actiontracker.Aspect.LoginAspectDecoder
import com.dell.dsc.eps.actiontracker.JsonEntity.*
import com.dell.dsc.eps.actiontracker.Mapper.ActionMapper
import com.dell.dsc.eps.actiontracker.Model.Action
import com.dell.dsc.eps.actiontracker.Model.ActionPatch
import com.dell.dsc.eps.actiontracker.Repository.IActionRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.util.*
import java.util.concurrent.CompletableFuture
import javax.transaction.Transactional
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import java.util.Calendar



@Service
class ActionService(val actionRepository: IActionRepository, val emailService: EmailService, val loginAspectDecoder: LoginAspectDecoder) {

    @Transactional
    fun saveAction(action: ActionEntity, timeOffset: Int): Action {
        val actionCount = countActionByIssueId(action.issueId)
        val generateActionId = action.issueId + "/" + (actionCount + 1).toString().padStart(2, '0')
        val assignerEmail = loginAspectDecoder.getLoggedInUserEmail()
        var updatedAction = ActionMapper().mapActionEntityToAction(action, assignerEmail);
        val action: Action = Action(updatedAction, actionId = generateActionId)
        updatedAction = actionRepository.save(action)
        val actionsForEmail: ArrayList<Action> = ArrayList<Action>()
        actionsForEmail.add(Action(updatedAction, action.dueDate - (timeOffset * 60 * 1000)))
        emailService.triggerEmail(getMailDataFromAction(action.assignedTo, assignerEmail, actionsForEmail))
        return updatedAction
    }

    @Transactional
    fun saveMultiAction(actions: List<ActionEntity>, timeOffset: Int): Iterable<Action> {
        var actionCount = countActionByIssueId(actions[0].issueId)
        val assignerEmail = loginAspectDecoder.getLoggedInUserEmail()
        val updatedActionList: ArrayList<Action> = ArrayList()
        val actionsForEmail = HashMap<String, ArrayList<Action>>()
        actions.forEach {
            if (it.actionId == null || it.actionId == "") {
                val generateActionId = it.issueId + "/" + (++actionCount).toString().padStart(2, '0')
                val updatedAction = Action(ActionMapper().mapActionEntityToAction(it, assignerEmail), actionId = generateActionId)
                updatedActionList.add(updatedAction)
                val actionsForSingleEmail: ArrayList<Action> = ArrayList<Action>()
                if (actionsForEmail.get(updatedAction.assignedTo) != null) {
                    actionsForSingleEmail.addAll(actionsForEmail.get(it.assignedTo) as ArrayList<Action>)
                }
                actionsForSingleEmail.add(updatedAction.copy(actionId = generateActionId, dueDate = it.dueDate - (timeOffset * 60 * 1000)))
                actionsForEmail.put(it.assignedTo, actionsForSingleEmail)
            } else {
                updatedActionList.add(ActionMapper().mapActionEntityToAction(it, assignerEmail))
            }
        }
        val actions = actionRepository.saveAll(updatedActionList)
        actionsForEmail.forEach { email, action ->
            emailService.triggerEmail(getMailDataFromAction(email, assignerEmail, action))
        }
        return actions
    }

    fun getAllAction(searchCriteria: SearchCriteria): PageableEntity {
        val pageRequest = PageRequest.of(searchCriteria.pageNo!!, searchCriteria.pageSize!!,
                Sort.by("status").descending().and(Sort.by("due_date").ascending()))

        val result: Page<Action>
        if (!searchCriteria.isDateEmpty()) {
            val updatedSearchCriteria: SearchCriteria = createSearchObj(searchCriteria)
            if (searchCriteria.startDate!!.equals(searchCriteria.endDate)) {
                searchCriteria.endDate = searchCriteria.endDate!! + 86399000
            }
            result = actionRepository.findAllByDueDateBetweenAndSourceAppLikeAndStatusLikeAndActionIdLike(
                    searchCriteria.startDate!!,
                    searchCriteria.endDate!!,
                    updatedSearchCriteria.sourceApp,
                    updatedSearchCriteria.status,
                    updatedSearchCriteria.actionId,
                    updatedSearchCriteria.issueId,
                    updatedSearchCriteria.emailId,
                    pageRequest)
        } else {
            val updatedSearchCriteria: SearchCriteria = createSearchObj(searchCriteria)
            result = actionRepository.findAllBySourceAppLikeAndStatusLikeAndActionIdLike(
                    updatedSearchCriteria.sourceApp,
                    updatedSearchCriteria.status,
                    updatedSearchCriteria.actionId,
                    updatedSearchCriteria.issueId,
                    updatedSearchCriteria.emailId,
                    pageRequest
            )
        }

        return PageableEntity(offset = result.pageable.offset, totalElements = result.totalElements,
                pageSize = result.pageable.pageSize, pageNumber = result.pageable.pageNumber,
                content = result.content)
    }

    fun findActionById(actionId: String): Optional<Action> {
        var action: Optional<Action> = actionRepository.findById(actionId)
        val currentDate = System.currentTimeMillis()

        // val sdf = SimpleDateFormat("dd/MM/yyyy")
        // val currentDateOnly = sdf.format(Date(currentDate))
        // var dueDateOnly = sdf.format(Date(action.get().dueDate))

        if (!action.isEmpty && action.get().dueDate < currentDate && action.get().status.toLowerCase() == "open") {
            action = Optional.of(actionRepository.save(action.get().copy(status = "delayed")))
        } else if (!action.isEmpty && action.get().dueDate > currentDate && action.get().status.toLowerCase() == "delayed") {
            action = Optional.of(actionRepository.save(action.get().copy(status = "open")))
        }

        // if (!action.isEmpty && dueDateOnly.compareTo(currentDateOnly) <= 0 && action.get().status.toLowerCase() == "open") {
        // action = Optional.of(actionRepository.save(action.get().copy(status = "delayed")))
        // } else if (!action.isEmpty && dueDateOnly.compareTo(currentDateOnly) > 0 && action.get().status.toLowerCase() == "delayed") {
        // action = Optional.of(actionRepository.save(action.get().copy(status = "open")))
        // }
        return action
    }

    fun findActionByIssueId(issueId: String): Iterable<Action> {
        val sort: Sort = Sort.by("status").descending().and(Sort.by("dueDate").ascending())
        // val pageRequest = PageRequest.of(searchCriteria.pageNo!!, searchCriteria.pageSize!!, Sort.by("status").descending().and(Sort.by("dueDate")).ascending())
        // return checkAndUpdateStatus(actionRepository.findByIssueId(issueId, sort))
        // ToDo we have to modify this api for updating the status
        return (actionRepository.findByIssueId(issueId, sort))
    }

    fun findByMultipleIssueId(issueIds: List<String>): HashMap<String, Iterable<Action>> {
        val actionListForIssues = HashMap<String, Iterable<Action>>()
        for (issue in issueIds) {
            actionListForIssues.put(issue, findActionByIssueId(issue))
        }
        return actionListForIssues
    }

    fun findUniqueAssigner(): List<FilterDropdown> {
        val uniqueAssigner = actionRepository.findAllAssigner().distinct().sorted()
        val result: ArrayList<FilterDropdown> = ArrayList<FilterDropdown>()
        uniqueAssigner.forEach {
            result.add(FilterDropdown(it, it))
        }
        return result
    }

    fun countActionByIssueId(issueId: String): Int {
        return actionRepository.countByIssueId(issueId)
    }

    /* This Service is used for update Single action */
    fun updateAction(action: Action): Action {
        val updatedAction = action.copy(updatedBy = loginAspectDecoder.getLoggedInUserEmail())
        return actionRepository.save(updatedAction)
    }

    @Transactional
    fun updateAction(actionId: String, status: String): Int {
        return actionRepository.updateStatus(actionId, status)
    }

    @Transactional
    fun updateAll(actionsUpdates: ArrayList<ActionPatch>): CompletableFuture<Boolean> {
        val completableFutures: ArrayList<CompletableFuture<Boolean>> = ArrayList()
        var completableFuture: CompletableFuture<Boolean>
        actionsUpdates.forEach {
            completableFuture = this.fetchAndUpdateAction(it)
            completableFutures.add(completableFuture)
        }
        val join = CompletableFuture.allOf(completableFutures.get(0)).join()
        return completableFutures.get(0)
    }

    @Async
    fun fetchAndUpdateAction(actionPatch: ActionPatch): CompletableFuture<Boolean> {
        actionPatch.actionPatch.forEach { action ->
            val retrievedAction = actionRepository.findByActionId(actionPatch.issueId, action.actionId)
            if (retrievedAction != null) {
                actionRepository.updateStatus(retrievedAction.actionId!!, action.status)
            }
        }
        return CompletableFuture.completedFuture(true)
    }

   /* fun checkAndUpdateStatus(actions: Iterable<Action>): Iterable<Action> {
        val currentDateOnly = Calendar.getInstance()
        currentDateOnly.set(Calendar.HOUR_OF_DAY, 0)
        currentDateOnly.set(Calendar.MINUTE, 0)
        currentDateOnly.set(Calendar.SECOND, 0)
        currentDateOnly.set(Calendar.MILLISECOND, 0)

        println("current date from calender...................")
        println(currentDateOnly.getTime())
        println("current date ends.............")

        val updatedList: ArrayList<Action> = ArrayList()
        actions.forEach {
            val dueDateOnly = Calendar.getInstance()
            dueDateOnly.setTime(Date(it.dueDate))
            dueDateOnly.set(Calendar.HOUR_OF_DAY, 0)
            dueDateOnly.set(Calendar.MINUTE, 0)
            dueDateOnly.set(Calendar.SECOND, 0)
            dueDateOnly.set(Calendar.MILLISECOND, 0)
            println("dueDateOnly from calender...................")
            println(it.issueId)
            println(dueDateOnly.getTime())
            println("dueDateOnly ends..............")
            if (dueDateOnly.getTimeInMillis() < currentDateOnly.getTimeInMillis() && it.status.toLowerCase() == "open") {
                updatedList.add(it.copy(status = "delayed"))
            } else if (dueDateOnly.getTimeInMillis() >= currentDateOnly.getTimeInMillis() && it.status.toLowerCase() == "delayed") {
                updatedList.add(it.copy(status = "open"))
            } else {
                updatedList.add(it.copy())
            }
        }
        return actionRepository.saveAll(updatedList)
    }*/

    fun createSearchObj(searchCriteria: SearchCriteria): SearchCriteria {
        if (searchCriteria.actionId != null && searchCriteria.actionId != "") {
            searchCriteria.actionId = "%" + searchCriteria.actionId + "%"
        }
        if (searchCriteria.actionId == "")
            searchCriteria.actionId = null

        if (searchCriteria.issueId == "")
            searchCriteria.issueId = null

        if (searchCriteria.issueId != null && searchCriteria.issueId != "") {
            searchCriteria.issueId = "%" + searchCriteria.issueId + "%"
        }

        if (searchCriteria.emailId == "")
            searchCriteria.emailId = null

        if (searchCriteria.emailId != null && searchCriteria.emailId != "") {
            searchCriteria.emailId = searchCriteria.emailId
        }

        if (searchCriteria.status == "all") {
            searchCriteria.status = null
        }
        if (searchCriteria.sourceApp == "all") {
            searchCriteria.sourceApp = null
        }
        return searchCriteria
    }

    fun getMailDataFromAction(assignee: String, assigner: String, actions: ArrayList<Action>): MailData {
        val emailBody: String
        val fromEmailId = "epsrca@dell.com"
        val emailSubject = "New Action has been assigned - Needs attention"
        var toEmailIds = assignee
        var actionTrackerLink = "https://partsy-ui-dev.ausvdc02.pcf.dell.com/action-tracker"
        if(System.getenv("APP_ORIGIN") != null) {
            actionTrackerLink = System.getenv("APP_ORIGIN") + "/action-tracker"
        }
        if (!assigner.toLowerCase().equals(assignee.toLowerCase())) {
            toEmailIds += ", " + assigner
        }
        if (actions.size == 1) {
            emailBody = emailService.getEmailBody(emailService.getActionRowTemplate(actions[0]), actionTrackerLink)
        } else {
            emailBody = emailService.getEmailBody(emailService.getActionRowTemplate(actions), actionTrackerLink)
        }
        return MailData(toEmailIds, fromEmailId, emailSubject, emailBody)
    }

}