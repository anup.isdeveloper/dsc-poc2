package com.dell.dsc.eps.actiontracker.Model

import java.sql.Date
import javax.persistence.Column
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "part_issue_details")
data class PartIssueDetails (
   @EmbeddedId
   val partIssueDetailsId:PartIssueDetailsId,
    @Column(name = "dit_commodity")
    val ditCommodity: String,
    @Column(name = "part_num")
    val partNo: String,
    @Column(name = "site")
    val site: String,
    @Column(name = "region")
    val region: String,
    @Column(name = "parent_commodity_code")
    val parentCommodityCode: String,
    @Column(name = "business_unit")
    val businessUnit: String,
    @Column(name = "lob")
    val lob: String,
    @Column(name = "cfg_name")
    val cfgName: String,
    @Column(name = "supplier")
    val supplier: String,
    @Column(name = "buyer_name")
    val buyerName: String,
    @Column(name = "buyer_email_id")
    val buyerEmailId: String,
    @Column(name = "report_date")
    val report_date: Date,
    @Column(name = "demand_region")
    val demandRegion: String,
    @Column(name = "root_cause")
    val rootCause: String,
    @Column(name = "use_case_specifics")
    val useCaseSpecifics: String
   ) {


}