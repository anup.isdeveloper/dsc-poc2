package com.dell.dsc.eps.actiontracker.JsonEntity

class MailData  {
     var maildata: EmailJson = EmailJson()

     constructor(toEmail: String,fromEmail:String,subject:String,body:String){
          this.maildata.toEmailId = toEmail
          this.maildata.fromEmailId = fromEmail
          this.maildata.bodyText = body
          this.maildata.subject = subject
     }

     class EmailJson {
          var toEmailId: String = ""
          var fromEmailId: String = ""
          var subject: String = ""
          var bodyText: String = ""
     }
}

