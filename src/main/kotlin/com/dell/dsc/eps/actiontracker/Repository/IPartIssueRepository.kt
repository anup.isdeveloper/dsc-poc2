package com.dell.dsc.eps.actiontracker.Repository

import com.dell.dsc.eps.actiontracker.Model.PartIssueDetails
import com.dell.dsc.eps.actiontracker.Model.PartIssueDetailsId
import org.springframework.data.jpa.repository.JpaRepository

interface IPartIssueRepository: JpaRepository<PartIssueDetails,PartIssueDetailsId> {
}