package com.dell.dsc.eps.actiontracker.Mapper

import com.dell.dsc.eps.actiontracker.JsonEntity.ActionEntity
import com.dell.dsc.eps.actiontracker.Model.Action

class ActionMapper {

    fun mapActionEntityToAction(action: ActionEntity, createdBy: String): Action {
        return Action(actionId = action.actionId, issueId = action.issueId, actionDescription = action.actionDescription, classification =  action.classification, sourceApp = action.sourceApp, issueType =  action.issueType, status =  action.status,
                assignedTo = action.assignedTo, dueDate = action.dueDate, lastUpdatedDate = action.lastUpdatedDate, createDate = action.createDate,  createdBy =  createdBy, comments =  action.comments!!, updatedBy = createdBy)
    }
}