package com.dell.dsc.eps.actiontracker.Configuration

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement

import javax.persistence.EntityManagerFactory
import javax.sql.DataSource

import java.util.Properties


@Configuration
@Profile("cloud")
@EnableJpaRepositories(entityManagerFactoryRef = "relationalManagerFactory", transactionManagerRef = "relationalTransactionManager", basePackages = ["com.dell.dsc.eps.actiontracker.Repository"])
@EnableTransactionManagement
class SqlCloudConfig {

    @Primary
    @Bean(name = ["relationalManagerFactory"])
    fun relationalManagerFactory(builder: EntityManagerFactoryBuilder,
                                 @Qualifier("memsql-db") dataSource: DataSource): LocalContainerEntityManagerFactoryBean {
        val entityManagerFactory = LocalContainerEntityManagerFactoryBean()

        entityManagerFactory.dataSource = dataSource
        entityManagerFactory.setPackagesToScan("com.dell.dsc.eps.actiontracker.Model")
        val vendorAdapter = HibernateJpaVendorAdapter()
        entityManagerFactory.jpaVendorAdapter = vendorAdapter
        val additionalProperties = Properties()
        additionalProperties["hibernate.show_sql"] = true
        entityManagerFactory.setJpaProperties(additionalProperties)
        return entityManagerFactory
    }

    @Primary
    @Bean(name = ["relationalTransactionManager"])
    fun relationalTransactionManager(@Qualifier("relationalManagerFactory")
                                     entityManagerFactory: EntityManagerFactory): PlatformTransactionManager {
        return JpaTransactionManager(entityManagerFactory)
    }
}