package com.dell.dsc.eps.actiontracker.Repository

import com.dell.dsc.eps.actiontracker.Model.Action
import org.springframework.data.domain.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*
import java.awt.print.Book
import javax.persistence.TypedQuery
import java.util.ArrayList
import javax.persistence.criteria.Root
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.EntityManager


interface IActionRepository : JpaRepository<Action, String> {

    @Query("select new Action( a.actionId, a.issueId, a.actionDescription, a.classification, a.sourceApp, a.issueType," +
            "a.status, a.assignedTo, a.dueDate, a.lastUpdatedDate, a.createDate, a.createdBy, a.updatedBy, a.comments) from Action a where  a.issueId = ?1")
    fun findByIssueId(issueId: String, sort: Sort): Iterable<Action>

    fun countByIssueId(issueId: String): Int


    @Query("select new Action( a.actionId, a.issueId, a.actionDescription, a.classification, a.sourceApp, a.issueType," +
            "a.status, a.assignedTo, a.dueDate, a.lastUpdatedDate, a.createDate, a.createdBy, a.updatedBy, a.comments) from Action a where a.assignedTo=?1 ")
    fun findAll(email: String, pageable: Pageable): Page<Action>

    @Modifying
    @Query("update Action a set a.status=?2 where a.actionId=?1")
    fun updateStatus(actionId: String, status: String): Int

    @Query("select new Action( a.actionId, a.issueId, a.actionDescription, a.classification, a.sourceApp, a.issueType," +
            "a.status, a.assignedTo, a.dueDate, a.lastUpdatedDate, a.createDate, a.createdBy, a.updatedBy, a.comments) from Action a   where a.issueId = ?1")
    fun findAllByIssueId(issueId: String): Iterable<Action>

    @Query("select LOWER(assigned_to) from action", nativeQuery = true)
    fun findAllAssigner(): List<String>

    @Query("select new Action(a.actionId, a.issueId, a.actionDescription, a.classification, a.sourceApp, a.issueType," +
            "a.status, a.assignedTo, a.dueDate, a.lastUpdatedDate, a.createDate, a.createdBy, a.updatedBy, a.comments) from Action a  where a.issueId = ?1 and a.actionId = ?2")
    fun findByActionId(issueId: String, actionId: String): Action

    @Query("Select action_id,issue_id,action_description,classification,source_app,issue_type,status,assigned_to," +
            "due_date,create_date,last_updated_date, created_by, updated_by, comments from action where due_date>= ?1 and due_date<= ?2 and source_app= COALESCE(?3, source_app) and status = COALESCE(?4,status) and action_id like COALESCE(?5,action_id) " +
            "and issue_Id like COALESCE(?6,issue_Id) and assigned_to= COALESCE(?7,assigned_to) ", countQuery = "Select count(*) from action where due_date>= ?1 and due_date<= ?2 and source_app= COALESCE(?3, source_app) and status = COALESCE(?4,status) and action_id like COALESCE(?5,action_id) " +
            "and issue_Id like COALESCE(?6,issue_Id) and assigned_to= COALESCE(?7,assigned_to) ", nativeQuery = true)
    fun findAllByDueDateBetweenAndSourceAppLikeAndStatusLikeAndActionIdLike(startDate: Long, endDate: Long, sourceApp: String?, status: String?, actionId: String?, issueId: String?, emailId: String?, pageable: Pageable): Page<Action>

    @Query("Select action_id,issue_id,action_description,classification,source_app,issue_type,status,assigned_to," +
            "due_date,create_date,last_updated_date, created_by, updated_by, comments from action where source_app= COALESCE(?1, source_app) and status = COALESCE(?2,status) and action_id like COALESCE(?3,action_id) " +
            "and issue_Id like COALESCE(?4,issue_Id) and assigned_to = COALESCE(?5,assigned_to) ", countQuery = "Select count(*) from action where source_app= COALESCE(?1, source_app) and status = COALESCE(?2,status) and action_id like COALESCE(?3,action_id) " +
            "and issue_Id like COALESCE(?4,issue_Id) and assigned_to = COALESCE(?5,assigned_to)", nativeQuery = true)
    fun findAllBySourceAppLikeAndStatusLikeAndActionIdLike(sourceApp: String?, status: String?, actionId: String?, issueId: String?, emailId: String?, pageable: Pageable): Page<Action>

}