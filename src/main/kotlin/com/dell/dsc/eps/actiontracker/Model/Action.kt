package com.dell.dsc.eps.actiontracker.Model

import javax.persistence.*

@Entity
@Table(name="action")
data class Action(

        @Id
        @Column(name = "action_id")
        val actionId: String? = null,
        @Column(name = "issue_id")
        val issueId: String,
        @Column(name = "action_description")
        val actionDescription: String,
        val classification: String,
        @Column(name = "source_app")
        val sourceApp: String? = null,
        @Column(name = "issue_type")
        val issueType: String? = null,
        val status: String = "OPEN",
        @Column(name = "assigned_to")
        val assignedTo: String,
        @Column(name = "due_date")
        val dueDate: Long,
        @Column(name = "last_updated_date")
        val lastUpdatedDate: Long,
        @Column(name = "create_date")
        val createDate: Long,
        @Column(name = "created_by")
        val createdBy: String,
        @Column(name = "updated_by")
        val updatedBy: String? = "",
        @Column(name = "comments")
        val comments: String? = ""

) {


    constructor(someAction: Action, actionId: String) : this(actionId = actionId,
            issueId = someAction.issueId, actionDescription = someAction.actionDescription,
            classification = someAction.classification, sourceApp = someAction.sourceApp,
            issueType = someAction.issueType, status = someAction.status, assignedTo = someAction.assignedTo,
            dueDate = someAction.dueDate, lastUpdatedDate = someAction.lastUpdatedDate, createDate = someAction.createDate,
            createdBy = someAction.createdBy, comments = someAction.comments, updatedBy = someAction.updatedBy)

    constructor(someAction: Action, dueDate: Long) : this(actionId = someAction.actionId,
            issueId = someAction.issueId, actionDescription = someAction.actionDescription,
            classification = someAction.classification, sourceApp = someAction.sourceApp,
            issueType = someAction.issueType, status = someAction.status, assignedTo = someAction.assignedTo,
            dueDate = dueDate, lastUpdatedDate = someAction.lastUpdatedDate, createDate = someAction.createDate,
            createdBy = someAction.createdBy, comments = someAction.comments, updatedBy = someAction.updatedBy)

}