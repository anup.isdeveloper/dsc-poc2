package com.dell.dsc.eps.actiontracker.ServiceTests

import com.dell.dsc.eps.actiontracker.Aspect.LoginAspectDecoder
import com.dell.dsc.eps.actiontracker.JsonEntity.*
import com.dell.dsc.eps.actiontracker.Model.Action
import com.dell.dsc.eps.actiontracker.Model.ActionPatch
import com.dell.dsc.eps.actiontracker.Repository.IActionRepository
import com.dell.dsc.eps.actiontracker.Service.ActionService
import com.dell.dsc.eps.actiontracker.Service.EmailService
import com.dell.dsc.eps.actiontracker.util.ConsumerClientTemplate
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.*
import org.springframework.data.domain.*
import org.springframework.data.domain.PageImpl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.util.ArrayList
import java.util.concurrent.CompletableFuture


@RunWith(MockitoJUnitRunner::class)
class ActionServiceTest {

    @Mock
    private lateinit var actionRepository: IActionRepository
    private lateinit var actionService: ActionService
    private lateinit var emailService: EmailService
    private lateinit var loginAspectDecoder: LoginAspectDecoder

    @Before
    fun setup() {
        emailService = EmailService(ConsumerClientTemplate())
        loginAspectDecoder = LoginAspectDecoder()
        actionService = ActionService(actionRepository, emailService, loginAspectDecoder)
    }


    fun createActionService(actionRepository: IActionRepository = mock(), emailService: EmailService = mock(), loginAspectDecoder: LoginAspectDecoder = mock())
            : ActionService {
        return ActionService(actionRepository, emailService, loginAspectDecoder)
    }


    @Test
    @Ignore
    fun canSaveOneAction() {
        val actionEntity = ActionEntity(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID")

        val action = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID", createdBy = "test")

        Mockito.`when`(actionRepository.save(org.mockito.ArgumentMatchers.any(Action::class.java))).thenReturn(action)

        var emailResponseEntity = ResponseEntity("Hello World!", HttpStatus.OK);
        val mockMailDataObject = MailData(body = "Some Body", toEmail = "test@dell.com", fromEmail = "partsy@dell.com",
                subject = "some Subject")
        Mockito.`when`(emailService.triggerEmail(mockMailDataObject)).thenReturn(emailResponseEntity)
        val result = actionService.saveAction(actionEntity, -330)
        assertThat(result).isEqualTo(action)
    }

    @Test
    @Ignore
    fun canSaveMultiAction() {
        val actionEntity1 = ActionEntity(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID")


        val actionEntity2 = ActionEntity(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID")

        val actionEntity3 = ActionEntity(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID")

        val actions = ArrayList<ActionEntity>()
        actions.add(actionEntity1)
        actions.add(actionEntity2)
        actions.add(actionEntity3)

        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID", createdBy = "test")

        val action2 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID", createdBy = "test")

        val actionsForSave = ArrayList<Action>()
        actionsForSave.add(action1)
        actionsForSave.add(action2)
        Mockito.`when`(actionRepository.saveAll(actionsForSave)).thenReturn(actionsForSave)
        val result = actionService.saveMultiAction(actions, -330)
        assertThat(result).isEqualTo(actionsForSave)
    }

    @Test
    @Ignore
    fun canGetAllActionsIfDateEmpty() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID", createdBy = "test")

        val action2 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID", createdBy = "test")

        val content = ArrayList<Action>()
        content.add(action1)
        content.add(action2)

        val pageableEntity = PageableEntity(content = content, offset = 0, pageNumber = 0, totalElements = 2, pageSize = 10)
        val searchCriteria = SearchCriteria(pageNo = pageableEntity.pageNumber, pageSize = pageableEntity.pageSize)

        val pageRequest = PageRequest.of(searchCriteria.pageNo!!, searchCriteria.pageSize!!,
                Sort.by("status").descending().and(Sort.by("due_date").ascending()))

        val pagedResponse = PageImpl(content, pageRequest, 2)

        Mockito.`when`(actionRepository.findAllBySourceAppLikeAndStatusLikeAndActionIdLike(sourceApp = searchCriteria.sourceApp, status = searchCriteria.status, actionId = searchCriteria.actionId,
                issueId = searchCriteria.issueId, emailId = searchCriteria.emailId, pageable = pageRequest)).thenReturn(pagedResponse)

        Mockito.`when`(actionRepository.saveAll(content)).thenReturn(content)

        val result = actionService.getAllAction(searchCriteria)
        assertThat(result.content).isEqualTo(pageableEntity.content)
    }

    @Test
    @Ignore
    fun canGetAllActionsIfDateSelected() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID", createdBy = "test")

        val action2 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID", createdBy = "test")

        val content = ArrayList<Action>()
        content.add(action1)
        content.add(action2)

        val pageableEntity = PageableEntity(content = content, offset = 0, pageNumber = 0, totalElements = 2, pageSize = 10)
        val searchCriteria = SearchCriteria(startDate = action1.dueDate - 100, endDate = action2.dueDate + 100, emailId = "test", pageNo = pageableEntity.pageNumber, pageSize = pageableEntity.pageSize)

        val pageRequest = PageRequest.of(searchCriteria.pageNo!!, searchCriteria.pageSize!!,
                Sort.by("status").descending().and(Sort.by("due_date").ascending()))

        val pagedResponse = PageImpl(content, pageRequest, 2)

        Mockito.`when`(actionRepository.findAllByDueDateBetweenAndSourceAppLikeAndStatusLikeAndActionIdLike(startDate = searchCriteria.startDate!!, endDate = searchCriteria.endDate!!, sourceApp = searchCriteria.sourceApp, status = searchCriteria.status, actionId = searchCriteria.actionId,
                issueId = searchCriteria.issueId, emailId = searchCriteria.emailId, pageable = pageRequest)).thenReturn(pagedResponse)

        Mockito.`when`(actionRepository.saveAll(content)).thenReturn(content)

        val result = actionService.getAllAction(searchCriteria)
        assertThat(result.content).isEqualTo(pageableEntity.content)
    }

    //@Test
    /*fun canFindActionById() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        Mockito.`when`(actionRepository.findById(action1.actionId!!)).thenReturn(Optional.of(action1))

        val result = actionService.findActionById(action1.actionId!!)
        assertThat(result).isEqualTo(Optional.of(action1))
    }*/

    @Test
    fun canFindActionByIssueId() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        val action2 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/02",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")
        val exceptedAction = ArrayList<Action>()
        exceptedAction.add(action1)
        exceptedAction.add(action2)

        val sort: Sort = Sort.by("status").descending().and(Sort.by("dueDate").ascending())
        Mockito.`when`(actionRepository.findByIssueId(action1.issueId, sort)).thenReturn(exceptedAction)

        val result = actionService.findActionByIssueId(action1.issueId)
        assertThat(result).isEqualTo(exceptedAction)
    }

    @Test
    fun canFindActionByMultipleIssueId() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        val action2 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/02",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        val action3 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "978/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "978", createdBy = "test")


        val actionsForIssue1 = ArrayList<Action>()
        actionsForIssue1.add(action1)
        actionsForIssue1.add(action2)

        val actionsForIssue2 = ArrayList<Action>()
        actionsForIssue2.add(action3)

        val issueIdList = ArrayList<String>()
        issueIdList.add("1234")
        issueIdList.add("978")

        val exceptedAction = HashMap<String, ArrayList<Action>>()
        exceptedAction.put(action1.issueId, actionsForIssue1)
        exceptedAction.put(action3.issueId, actionsForIssue2)

        val sort: Sort = Sort.by("status").descending().and(Sort.by("dueDate").ascending())
        Mockito.`when`(actionRepository.findByIssueId(action1.issueId, sort)).thenReturn(actionsForIssue1)
        Mockito.`when`(actionRepository.findByIssueId(action3.issueId, sort)).thenReturn(actionsForIssue2)

        val result = actionService.findByMultipleIssueId(issueIdList)
        assertThat(result).isEqualTo(exceptedAction)
    }

    @Test
    fun canFindUniqueAssigner() {
        val allAssigner = ArrayList<String>()
        allAssigner.add("test1@dell.com")
        allAssigner.add("test1@dell.com")
        allAssigner.add("test2@dell.com")
        allAssigner.add("test2@dell.com")
        allAssigner.add("test4@dell.com")
        allAssigner.add("test3@dell.com")

        val exceptedResult = ArrayList<FilterDropdown>()
        exceptedResult.add(FilterDropdown("test1@dell.com", "test1@dell.com"))
        exceptedResult.add(FilterDropdown("test2@dell.com", "test2@dell.com"))
        exceptedResult.add(FilterDropdown("test4@dell.com", "test4@dell.com"))
        exceptedResult.add(FilterDropdown("test3@dell.com", "test3@dell.com"))

        Mockito.`when`(actionRepository.findAllAssigner()).thenReturn(allAssigner)
        val result = actionService.findUniqueAssigner()

        assertThat(result.size).isEqualTo(4)
        //  assertThat(result).isEqualTo(exceptedResult)
    }

    @Test
    fun canCountActionByIssueId() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        val action2 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/02",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        val action3 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "978/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "978", createdBy = "test")

        Mockito.`when`(actionRepository.countByIssueId(action1.issueId)).thenReturn(2)

        val result = actionService.countActionByIssueId(action1.issueId)

        assertThat(result).isEqualTo(2)
    }

    /*@Test
    fun canUpdateAction() {
        val action1 = Action(actionId = "124", actionDescription = "Great Action", classification = "Very Low",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time,
                createDate = Date().time, issueId = "some issue", status = "open", createdBy = "test")

        val exceptedAction = Action(actionId = "124", actionDescription = "Great Action", classification = "Very Low",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time,
                createDate = Date().time, issueId = "some issue", status = "open", createdBy = "test")

        Mockito.`when`(actionRepository.save(exceptedAction)).thenReturn(exceptedAction)
      //  Mockito.`when`(loginAspectDecoder.getLoggedInUserEmail()).thenReturn("update_user@dell.com")
        val result = actionService.updateAction(action1)
        assertThat(result).isEqualTo(exceptedAction)
    }*/

    @Test
    fun canUpdateActionStatus() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        Mockito.`when`(actionRepository.updateStatus(action1.actionId!!, action1.status)).thenReturn(1)

        val result = actionService.updateAction(action1.actionId!!, action1.status)
        assertThat(result).isEqualTo(1)
    }

    @Test
    fun canUpdateAllActions() {

        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        val action2 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/02",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        val action3 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "978/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "978", createdBy = "test")

        val actionPatch = ArrayList<ActionPatch>()
        val actions1 = ArrayList<ActionPatch.ActionPatchObj>()
        actions1.add(ActionPatch.ActionPatchObj(action1.actionId!!, action1.status))
        actions1.add(ActionPatch.ActionPatchObj(action2.actionId!!, action2.status))

        val actions2 = ArrayList<ActionPatch.ActionPatchObj>()
        actions2.add(ActionPatch.ActionPatchObj(action3.actionId!!, action3.status))
        actionPatch.add(ActionPatch(action1.issueId, actions1))
        actionPatch.add(ActionPatch(action3.issueId, actions2))

        Mockito.`when`(actionRepository.findByActionId(action1.issueId, action1.actionId!!)).thenReturn(action1)
        Mockito.`when`(actionRepository.updateStatus(action1.actionId!!, action1.status)).thenReturn(1)

        val result = actionService.updateAll(actionPatch)

        // val completableFutures: ArrayList<CompletableFuture<Boolean>> = ArrayList()
        var completableFuture = CompletableFuture<Boolean>()
        completableFuture = CompletableFuture.completedFuture(true)
        assertThat(result.get()).isEqualTo(completableFuture.get())
    }

    @Test
    fun canCreateSearchObjForNotNull() {
        val searchCriteria = SearchCriteria(actionId = "1234/01", issueId = "1234", emailId = "test@dell.com", sourceApp = "all", status = "all")
        val exceptedSearchObj = SearchCriteria(actionId = "%" + searchCriteria.actionId + "%", issueId = "%" + searchCriteria.issueId + "%",
                emailId = searchCriteria.emailId, status = null, sourceApp = null)

        val result = actionService.createSearchObj(searchCriteria)

        assertThat(result.actionId).isEqualTo(exceptedSearchObj.actionId)
        assertThat(result.issueId).isEqualTo(exceptedSearchObj.issueId)
        assertThat(result.status).isEqualTo(exceptedSearchObj.status)
    }

    @Test
    fun canCreateSearchObjForNull() {
        val searchCriteria = SearchCriteria(actionId = "", issueId = "", emailId = "", sourceApp = "all", status = "all")
        val exceptedSearchObj = SearchCriteria(actionId = null, issueId = null, emailId = null, status = null, sourceApp = null)

        val result = actionService.createSearchObj(searchCriteria)

        assertThat(result.actionId).isEqualTo(exceptedSearchObj.actionId)
        assertThat(result.issueId).isEqualTo(exceptedSearchObj.issueId)
        assertThat(result.emailId).isEqualTo(exceptedSearchObj.emailId)
    }

    @Test
    fun canGetMailDataFromActionForSingleAction() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        val actions = ArrayList<Action>()
        actions.add(action1)

        val mailData = MailData(action1.assignedTo, action1.assignedTo, "New Action has been assigned - Needs attention", "")

        val result = actionService.getMailDataFromAction(action1.assignedTo, action1.assignedTo, actions)
        assertThat(result.maildata.toEmailId).isEqualTo(mailData.maildata.toEmailId)
        // assertThat(result.maildata.bodyText).isEqualTo(mailData.maildata.bodyText)
    }

    @Test
    fun canGetMailDataFromActionForMultipleAction() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        val action2 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "delayed", actionId = "1234/02",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", createdBy = "test")

        val actions = ArrayList<Action>()
        actions.add(action1)
        actions.add(action2)

        val mailData = MailData(action1.assignedTo, action1.assignedTo, "New Action has been assigned - Needs attention", "")

        val result = actionService.getMailDataFromAction(action1.assignedTo, action1.assignedTo, actions)
        assertThat(result.maildata.toEmailId).isEqualTo(mailData.maildata.toEmailId)
        // assertThat(result.maildata.bodyText).isEqualTo(mailData.maildata.bodyText)
    }
}