package com.dell.dsc.eps.actiontracker.ControllerTests

import com.dell.dsc.eps.actiontracker.Controller.CreateActionController
import com.dell.dsc.eps.actiontracker.JsonEntity.ActionEntity
import com.dell.dsc.eps.actiontracker.Model.Action
import com.dell.dsc.eps.actiontracker.Service.ActionService
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.*
import kotlin.collections.ArrayList

@RunWith(MockitoJUnitRunner::class)
class CreateActionControllerTest {

    @Mock
    private lateinit var actionService: ActionService

    @InjectMocks
    private lateinit var createActionController: CreateActionController

    @Test
    fun canCreateOneAction() {

        val firstAction = ActionEntity(actionDescription = "Great Action", classification = "Very Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID")

        val exceptedAction = Action(actionDescription = "Great Action", classification = "Very Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, actionId = firstAction.issueId + "/" + "01", issueId = "someIssueID", createdBy = "test")

        //     Mockito.`when`(actionService.countActionByIssueId(firstAction.issueId)).thenReturn(0)

        Mockito.`when`(actionService.saveAction(firstAction, -330)).thenReturn(exceptedAction)

        val result1 = createActionController.createAction(firstAction, -330)
        assertThat(result1).isEqualTo(exceptedAction)
    }

    @Test
    fun canCreateMultiActions() {
        val action1 = ActionEntity(actionDescription = "Great Action 1", classification = "Very Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID")
        val action2 = ActionEntity(actionDescription = "Great Action 2", classification = "Very Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID")

        val expectedAction1 = Action(actionDescription = "Great Action 1", classification = "Very Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, actionId = action1.issueId + "/" + "01", issueId = "someIssueID", createdBy = "test")
        val expectedAction2 = Action(actionDescription = "Great Action 2", classification = "Very Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, actionId = action1.issueId + "/" + "02", issueId = "someIssueID", createdBy = "test")

        val someActions: ArrayList<ActionEntity> = ArrayList()
        someActions.add(action1)
        someActions.add(action2)

        val exceptedActions: ArrayList<Action> = ArrayList()
        exceptedActions.add(expectedAction1)
        exceptedActions.add(expectedAction2)

        Mockito.`when`(actionService.saveMultiAction(someActions, -330)).thenReturn(exceptedActions)
        val results = createActionController.createMultiAction(someActions, -330)
        assertThat(results).usingElementComparatorIgnoringFields("id", "dueDate", "lastUpdatedDate", "createDate").containsExactlyInAnyOrder(expectedAction1, expectedAction2)
    }
}