package com.dell.dsc.eps.actiontracker.ServiceTests

import com.dell.dsc.eps.actiontracker.Aspect.LoginAspectDecoder
import com.dell.dsc.eps.actiontracker.JsonEntity.MailData
import com.dell.dsc.eps.actiontracker.Model.Action
import com.dell.dsc.eps.actiontracker.Service.EmailService
import com.dell.dsc.eps.actiontracker.util.ConsumerClientTemplate
import junit.framework.Assert
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpStatus
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@RunWith(MockitoJUnitRunner::class)
class EmailServiceTest {

    private lateinit var emailService: EmailService
    private lateinit var loginAspectDecoder: LoginAspectDecoder

    @Before
    fun setup(){
        emailService = EmailService(ConsumerClientTemplate())
        loginAspectDecoder = LoginAspectDecoder()
    }

    @Test
    fun canGetRowTemplateForOneAction() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low",
                sourceApp = "STBL", status = "delayed", actionId = "myIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time,
                issueId = "someIssueID",  createdBy = "test")

        val formatter = SimpleDateFormat("MMM dd, yyyy")
        val expectedDueDate = formatter.format(Date(action1.dueDate))


        val expected = "<tr style='text-align: center'> <td style='font-size: 15px;padding: 10px 5px;'>${action1.issueId}</td>" +
        "<td style='font-size: 15px;padding: 10px 5px;'>01</td> <td style='font-size: 15px;" +
                "padding: 10px 5px;'>${action1.actionDescription}</td> <td style='font-size: 15px;padding: 10px 5px;'>$expectedDueDate</td>" +
                "<td style='font-size: 15px;padding: 10px 5px;'>${action1.status}</td> </tr>"

        val actual = emailService.getActionRowTemplate(action1)
        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun canGetRowTemplateForMultipleActions() {
        val action1 = Action(actionDescription = "This action", classification = "Low",
                sourceApp = "STBL", status = "delayed", actionId = "myIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time,
                issueId = "someIssueID",  createdBy = "test")

        val action2 = Action(actionDescription = "My other action", classification = "High",
                sourceApp = "COS RED", status = "Open", actionId = "myIssueID/02",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time,
                issueId = "someOtherIssueID",  createdBy = "test")

        val formatter = SimpleDateFormat("MMM dd, yyyy")
        val expectedDueDateForAction1 = formatter.format(Date(action1.dueDate))
        val expectedDueDateForAction2 = formatter.format((Date(action2.dueDate)))

        val actions: ArrayList<Action> = ArrayList()
        actions.add(action1)
        actions.add(action2)

        val expected = "<tr style='text-align: center'> <td style='font-size: 15px;padding: 10px 5px;'>${action1.issueId}</td>" +
                "<td style='font-size: 15px;padding: 10px 5px;'>01</td> <td style='font-size: 15px;" +
                "padding: 10px 5px;'>${action1.actionDescription}</td> <td style='font-size: 15px;padding: 10px 5px;'>$expectedDueDateForAction1</td>" +
                "<td style='font-size: 15px;padding: 10px 5px;'>${action1.status}</td> </tr>" +

                "<tr style='text-align: center'> <td style='font-size: 15px;padding: 10px 5px;'>${action2.issueId}</td>" +
                "<td style='font-size: 15px;padding: 10px 5px;'>02</td> <td style='font-size: 15px;" +
                "padding: 10px 5px;'>${action2.actionDescription}</td> <td style='font-size: 15px;padding: 10px 5px;'>$expectedDueDateForAction2</td>" +
                "<td style='font-size: 15px;padding: 10px 5px;'>${action2.status}</td> </tr>"

        val actual = emailService.getActionRowTemplate(actions)
        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun canGetEmailBodyForOneAction() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low",
                sourceApp = "STBL", status = "delayed", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time,
                issueId = "someIssueID",  createdBy = "test")

        val expectedRowTemplate = emailService.getActionRowTemplate(action1)
        val actionTrackerLink = "https://partsy-ui-dev.ausvdc02.pcf.dell.com" + "/action-tracker"

        val expectedEmailBody = "<body style='text-align: -webkit-center;'>\n" +
                "\n" +
                "<div style='text-align: center; width: 1000px; border: 1px solid #8dacc5;  padding-bottom: 50px;'>\n" +
                "    <div style='background-color: #00447c; height: 100px '>\n" +
                "        <div style='padding-top: 30px'>\n" +
                "            <span style='color: #fff; font-size: 32px'>Root Cause & Action Engine</span>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div style='padding-top: 20px'>\n" +
                "        <table style='width: 100%'>\n" +
                "            <thead>\n" +
                "            <tr>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>ISSUE ID</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>ACTION ID</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>ACTION</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>DUE DATE</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>STATUS</th>\n" +
                "            </tr>\n" +
                "            </thead>\n" +
                "            <tbody>$expectedRowTemplate</tbody>\n" +
                "        </table>\n" +
                "\n" +
                "    </div>\n" +
                "\n" +
                "    <div style='padding-top: 50px'>\n" +
                "        <div style='text-align: center'>\n" +
                "            <button style='padding: 10px 20px; font-size: 14px; background-color: #00447c !important; border: none; border-radius: 3px;'>\n" +
                "                <a style='color: white !important;text-decoration: none;' href='$actionTrackerLink'>Goto Action View</a>\n" +
                "            </button>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n" +
                "</body>"

        val emailBody = emailService.getEmailBody(emailService.getActionRowTemplate(action1), actionTrackerLink)
        Assert.assertEquals(emailBody, expectedEmailBody)
    }

    @Test
    fun canGetEmailBodyForMultipleAction() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low",
                sourceApp = "STBL", status = "delayed", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time,
                issueId = "someIssueID",  createdBy = "test")

        val action2 = Action(actionDescription = "My other action", classification = "High",
                sourceApp = "COS RED", status = "Open", actionId = "myIssueID/02",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time,
                issueId = "someOtherIssueID",  createdBy = "test")

        val actions: ArrayList<Action> = ArrayList()
        actions.add(action1)
        actions.add(action2)

        val expectedRowTemplate = emailService.getActionRowTemplate(actions)
        val actionTrackerLink = "https://partsy-ui-dev.ausvdc02.pcf.dell.com" + "/action-tracker"

        val expectedEmailBody = "<body style='text-align: -webkit-center;'>\n" +
                "\n" +
                "<div style='text-align: center; width: 1000px; border: 1px solid #8dacc5;  padding-bottom: 50px;'>\n" +
                "    <div style='background-color: #00447c; height: 100px '>\n" +
                "        <div style='padding-top: 30px'>\n" +
                "            <span style='color: #fff; font-size: 32px'>Root Cause & Action Engine</span>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div style='padding-top: 20px'>\n" +
                "        <table style='width: 100%'>\n" +
                "            <thead>\n" +
                "            <tr>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>ISSUE ID</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>ACTION ID</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>ACTION</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>DUE DATE</th>\n" +
                "                <th style='font-size: 15px; padding: 15px 5px; background-color: #f4f4f4'>STATUS</th>\n" +
                "            </tr>\n" +
                "            </thead>\n" +
                "            <tbody>$expectedRowTemplate</tbody>\n" +
                "        </table>\n" +
                "\n" +
                "    </div>\n" +
                "\n" +
                "    <div style='padding-top: 50px'>\n" +
                "        <div style='text-align: center'>\n" +
                "            <button style='padding: 10px 20px; font-size: 14px; background-color: #00447c !important; border: none; border-radius: 3px;'>\n" +
                "                <a style='color: white !important;text-decoration: none;' href='$actionTrackerLink'>Goto Action View</a>\n" +
                "            </button>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n" +
                "</body>"

        val emailBody = emailService.getEmailBody(emailService.getActionRowTemplate(actions), actionTrackerLink)
        Assert.assertEquals(emailBody, expectedEmailBody)
    }

    @Test
    fun canTriggerEmailWithValidData() {
        val someMailData = MailData("some_1@dell.com", "epsrca@dell.com", "Need Action", "Action Body")
        val result = emailService.triggerEmail(someMailData)
        assertThat(result.statusCode).isEqualTo(HttpStatus.OK)
    }
}