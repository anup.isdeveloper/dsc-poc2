package com.dell.dsc.eps.actiontracker.ControllerTests

import com.dell.dsc.eps.actiontracker.Controller.RetrieveActionController
import com.dell.dsc.eps.actiontracker.JsonEntity.FilterDropdown
import com.dell.dsc.eps.actiontracker.JsonEntity.Issues
import com.dell.dsc.eps.actiontracker.JsonEntity.PageableEntity
import com.dell.dsc.eps.actiontracker.JsonEntity.SearchCriteria
import com.dell.dsc.eps.actiontracker.Model.Action
import com.dell.dsc.eps.actiontracker.Service.ActionService
import junit.framework.Assert.assertEquals
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


@RunWith(MockitoJUnitRunner::class)
class RetrieveActionControllerTest {

    @Mock
    private lateinit var actionService: ActionService

    @InjectMocks
    private lateinit var retrieveActionController: RetrieveActionController

    @Test
    fun canGetOneAction() {
        val action1 = Action(actionId = "0f14d0ab-9605-4a62-a9e4-5ed26688389b", actionDescription = "Retrieve this Action", classification = "Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID", createdBy = "test")

        Mockito.`when`(actionService.findActionById(action1.actionId!!)).thenReturn(Optional.of(action1))
        val result = retrieveActionController.getActionById(action1.actionId!!)
        assertThat(result).isEqualTo(Optional.of(action1))
    }

    @Test
    fun canGetMultipleActions() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID",  createdBy = "test")
        val action2 = Action(actionDescription = "Retrieve this Action2", classification = "Medium", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID",  createdBy = "test")
        val action3 = Action(actionDescription = "Retrieve this Action3", classification = "Medium", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID",  createdBy = "test")
        val actions: ArrayList<Action> = ArrayList<Action>()
        actions.add(action1)
        actions.add(action2)
        actions.add(action3)

        val searchCriteria = SearchCriteria()
        val pageableEntity = PageableEntity(offset = 0, pageNumber = 0, pageSize = 10, totalElements = 50, content = actions)

        Mockito.`when`(actionService.getAllAction(searchCriteria)).thenReturn(pageableEntity)
        val result: List<Any> = retrieveActionController.getActions(searchCriteria).content.toList()
        assertThat(result.size).isEqualTo(3)
        assertEquals(result, actions)
    }

    @Test
    fun canFindActionByActionId() {
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID",  createdBy = "test")

        Mockito.`when`(actionService.findActionById("0f14d0ab-9605-4a62-a9e4-5ed26688389b")).thenReturn(Optional.of(action1))
        val result = retrieveActionController.getActionById("0f14d0ab-9605-4a62-a9e4-5ed26688389b")

        assertThat(result).isEqualTo(Optional.of(action1))
    }

    @Test
    fun canFindActionsForOneIssueId() {
        val action1 = Action(actionDescription = "Retrieve this Action 1", classification = "Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", status = "open", createdBy = "test")

        val issue = Issues(issueId = "1234")

        val action2 = Action(actionDescription = "Retrieve this Action 2", classification = "Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234", status = "open", createdBy = "test")

        val actions: ArrayList<Action> = ArrayList()
        actions.add(action1)
        actions.add(action2)

        Mockito.`when`(actionService.findActionByIssueId(action1.issueId)).thenReturn(actions)
        val result = retrieveActionController.getActionsForOneIssueId(issue).toList()

        assertEquals(result, actions)
        assertThat(result).isEqualTo(actions)
    }

    @Test
    fun canFindActionsForMultipleIssueIds() {
        val action1 = Action(actionDescription = "Retrieve this Action 1", classification = "Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234",  createdBy = "test")
        val action2 = Action(actionDescription = "Retrieve this Action 2", classification = "Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "1234",  createdBy = "test")
        val action3 = Action(actionDescription = "Retrieve this Action 3", classification = "Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "12345",  createdBy = "test")

        val expectedActions = HashMap<String, Iterable<Action>>()
        val issueId1: ArrayList<Action> = ArrayList()
        issueId1.add(action1)
        issueId1.add(action2)

        val issueId2: ArrayList<Action> = ArrayList()
        issueId2.add(action3)

        expectedActions.put(action1.issueId, issueId1)
        expectedActions.put(action2.issueId, issueId2)

        val issueIDList: ArrayList<String> = ArrayList()
        issueIDList.add(action1.issueId)
        issueIDList.add(action3.issueId)

        Mockito.`when`(actionService.findByMultipleIssueId(issueIDList)).thenReturn(expectedActions)
        val result = retrieveActionController.getActionsForMultipleIssueId(issueIDList)

        // assertThat(result).usingElementComparatorIgnoringFields("dueDate", "lastUpdatedDate","createDate").containsExactlyInAnyOrder(action1, action2)
        assertEquals(result, expectedActions)
        assertThat(result).isEqualTo(expectedActions)
    }

    @Test
    fun canGetActionsForSearchCriteria(){
        val action1 = Action(actionDescription = "Retrieve this Action1", classification = "Low", sourceApp = "STBL", status = "Open", actionId = "someIssueID/01",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID",  createdBy = "test")
        val action2 = Action(actionDescription = "Retrieve this Action2", classification = "Medium", sourceApp = "STBL", status = "Delayed", actionId = "someIssueID/02",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID",  createdBy = "test")
        val action3 = Action(actionDescription = "Retrieve this Action3", classification = "Medium", sourceApp = "COS Red", status = "Completed", actionId = "someIssueID/03",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time, createDate = Date().time, issueId = "someIssueID",  createdBy = "test")

        val actions: ArrayList<Action> = ArrayList<Action>()
        actions.add(action1)
      //  actions.add(action2)
      //  actions.add(action3)

        val searchCriteria = SearchCriteria(startDate = Date().time.minus(10000),endDate = Date().time, sourceApp = "STBL", status="Open", actionId = "01")
        val pageableEntity = PageableEntity(offset = 0, pageNumber = 0, pageSize = 10, totalElements = 50, content = actions)

        Mockito.`when`(actionService.getAllAction(searchCriteria)).thenReturn(pageableEntity)
        val result: List<Any> = retrieveActionController.getActions(searchCriteria).content.toList()
        assertThat(result.size).isEqualTo(1)
        assertEquals(result[0], action1)
    }

    @Test
    fun canGetAllUniqueAssigner() {
        val allUniqueAssigner = ArrayList<FilterDropdown>()
        allUniqueAssigner.add(FilterDropdown("assigner1", "assigner1"))
        allUniqueAssigner.add(FilterDropdown("assigner2", "assigner2"))
        allUniqueAssigner.add(FilterDropdown("assigner3", "assigner3"))
        Mockito.`when`(actionService.findUniqueAssigner()).thenReturn(allUniqueAssigner)

        val result = retrieveActionController.getAllUniqueAssigner()
        assertThat(result).isEqualTo(allUniqueAssigner)
    }
}