package com.dell.dsc.eps.actiontracker.ControllerTests

import com.dell.dsc.eps.actiontracker.Controller.UpdateActionController
import com.dell.dsc.eps.actiontracker.JsonEntity.ResponseType
import com.dell.dsc.eps.actiontracker.Model.Action
import com.dell.dsc.eps.actiontracker.Model.ActionPatch
import com.dell.dsc.eps.actiontracker.Service.ActionService
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.util.*
import java.util.concurrent.CompletableFuture
import kotlin.collections.ArrayList

@RunWith(MockitoJUnitRunner::class)
class UpdateActionControllerTest {

    @Mock
    private lateinit var actionService: ActionService

    @InjectMocks
    private lateinit var updateActionController: UpdateActionController

    @Test
    fun canUpdateStatusOfOneAction() {
        val firstAction = Action(actionId = "124", actionDescription = "Great Action", classification = "Very Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "some issue", status = "open", createdBy = "test")

        val exceptedResult = ResponseEntity(ResponseType(returnMessage = "Success"), HttpStatus.CREATED)

        Mockito.`when`(actionService.updateAction(firstAction.actionId!!, firstAction.status)).thenReturn(1)

        val result = updateActionController.updateStatus(firstAction)
        assertThat(result.statusCode).isEqualTo(exceptedResult.statusCode)
    }

    @Test
    fun canUpdateStatusOfMultipleAction() {
        val actions: ArrayList<Action> = ArrayList()
        val firstAction = Action(actionId = "1234", actionDescription = "Great Action", classification = "Very Low", assignedTo = "Action Tracker", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "bogus issue 1", createdBy = "test")

        val secondAction = Action(actionId = "1234", actionDescription = "Great Action 2", classification = "Very Low", assignedTo = "Action Tracker 2", dueDate = Date().time,
                lastUpdatedDate = Date().time, createDate = Date().time, issueId = "bogus issue 2", createdBy = "test")

        actions.add(firstAction)
        actions.add(secondAction)

        val firstPatchObj = ActionPatch.ActionPatchObj(actions[0].actionId!!, status = "COMPLETED")
        val firstPatchObjList = ArrayList<ActionPatch.ActionPatchObj>()
        firstPatchObjList.add(firstPatchObj)

        val secondPatchObj = ActionPatch.ActionPatchObj(actions[1].actionId!!, status = "DELAYED")
        val secondPatchObjList = ArrayList<ActionPatch.ActionPatchObj>()
        secondPatchObjList.add(secondPatchObj)

        val patchActions: ArrayList<ActionPatch> = ArrayList()


        patchActions.add(ActionPatch(firstAction.issueId, firstPatchObjList))
        patchActions.add(ActionPatch(secondAction.issueId, secondPatchObjList))

        val exceptedResult = CompletableFuture<Boolean>()

        Mockito.`when`(actionService.updateAll(patchActions)).thenReturn(exceptedResult)

        val result = updateActionController.updateMultiStatus(patchActions)

        assertThat(result).isEqualTo(exceptedResult)
    }

    @Test
    fun canUpdateOneAction() {
        val firstAction = Action(actionId = "124", actionDescription = "Great Action", classification = "Very Low",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time,
                createDate = Date().time, issueId = "some issue", status = "open", createdBy = "test")

        val exceptedAction = Action(actionId = "124", actionDescription = "Great Action", classification = "Very Low",
                assignedTo = "Action Tracker", dueDate = Date().time, lastUpdatedDate = Date().time,
                createDate = Date().time, issueId = "some issue", status = "open", createdBy = "test",
                updatedBy = "update_user@dell.com")

       /* val exceptedAction = firstAction.copy(actionId = firstAction.actionId)*/

        Mockito.`when`(actionService.updateAction(firstAction)).thenReturn(exceptedAction)

        val result = updateActionController.updateAction(firstAction)
        assertThat(result).isEqualTo(exceptedAction)
    }
}