package com.dell.dsc.eps.actiontracker

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.context.annotation.Bean
import org.springframework.core.task.TaskExecutor
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor

@EnableAsync
@RunWith(MockitoJUnitRunner::class)
class ActionTrackerApplicationTests {

	@Test
	fun contextLoads() {
	}

}

@Bean
fun threadPoolTaskExecutor(): TaskExecutor {
	val executor = ThreadPoolTaskExecutor()
	executor.corePoolSize = Runtime.getRuntime().availableProcessors() - 1
	executor.maxPoolSize = Runtime.getRuntime().availableProcessors() - 1
	executor.setThreadNamePrefix("default_task_executor_thread")
	executor.initialize()
	return executor
}
